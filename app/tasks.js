const express = require('express');

const Task = require('../models/Task');
const auth = require('../middwares/auth');

const router = express.Router();

const createRouter = () => {
  router.get('/', auth, (req, res) => { // +
    Task.find({user: req.user._id}).populate('user')
      .then(result => res.send(result))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', auth, (req, res) => { // +
    const taskData = req.body;
    taskData.user = req.user;

    const task = new Task(taskData);

    task.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.put('/:id', auth, async (req, res) => { // +
    const taskData = req.body;
    const id = req.params.id;
    const userId = req.user._id;

    if (taskData.user) return res.send({error: 'You can not edit field "user"'});

    if (!taskData.user) {
      let task = await Task.findOne({_id: id});

      if (!task) return res.status(404).send({error: 'Task not found'});

      if (!task.user.equals(userId)) return res.status(403).send({error: 'You have not access to edit this task'});

      task.set(taskData);

      await task.save();

      res.send(task);
    }
  });

  router.delete('/:id', auth, async (req, res) => { // +
    const id = req.params.id;
    const userId = req.user._id;

    let task = await Task.findOne({_id: id});

    if (!task) return res.status(404).send({error: 'You try to delete nonexistent task'});

    if (!task.user.equals(userId)) return res.status(403).send({error: 'You have not access to delete this task'});

    await task.remove();

    res.send({message: 'Task deleted'});
  });

  return router;
};

module.exports = createRouter;